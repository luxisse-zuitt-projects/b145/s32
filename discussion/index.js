// Creating a simple server in Express.js

const express = require('express');
const mongoose = require('mongoose');
const app = express();

const port = 4000;

// Connecting MongoDB
mongoose.connect("mongodb+srv://luxisse:pr0wdmon@b145.gxg3t.mongodb.net/session32?retryWrites=true&w=majority", 
	{
		useNewUrlParser : true,
		useUnifiedTopology: true
	}
);

let db = mongoose.connection;

db.on("error", console.error.bind(console, "There is an error with the connection"))
db.once("open", () => console.log("Successfully connected to the database"))

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Mongoose Schemas
	// schema === blueprint of your data

const taskSchema = new mongoose.Schema({

	name: String,
	status: {
		type: String,
		default: 'pending'
	}

});


// S32 ACTIVITY USER SCHEMA
const userSchema = new mongoose.Schema({

	email: String,
	username: String,
	password: String,
	age: Number,
	isAdmin: {
		type: Boolean, 
		default: false
	}

});



// Model
const Task = mongoose.model("Task", taskSchema);

// S32 USER MODEL
const User = mongoose.model("User", userSchema);

// Business Logic

// Creating a new task 
app.post("/tasks", (req, res) => {

	Task.findOne({name : req.body.name}, (err, result) => {

		if(result != null && result.name === req.body.name){
			
			return res.send(`Duplicate task found: ${err}`)
		
		} else {
			
			let newTask = new Task({
				name: req.body.name
			});

			newTask.save((saveErr, savedTask) => {
				if(saveErr){
					return console.error(saveErr);
				} else {
					return res.status(200).send(`New task created: ${savedTask}`)
				};
			});
		};
	});
});


// Retrieving all tasks
app.get("/tasks", (req, res) => {

	Task.find({}, (err, result) => {
		
		if(err){
			
			return console.log(err);
		
		} else {
			
			return res.status(200).json({
				tasks: result
			})
		}	
	})
})

// Updating Task Name
app.put("/tasks/update/:taskId", (req, res) => {

	let taskId = req.params.taskId
	let name = req.body.name
	// let reqBody = req.body

	Task.findByIdAndUpdate(taskId, {name: name}, (err, updatedTask) => {
		if(err){
				console.log(err)
		} else {
				res.send(`Congratulations the task has been updated`);
		}
	});
});


// Delete Task
app.delete("/tasks/archive-task/:taskId", (req, res) => {

	let taskId = req.params.taskId;
	
	Task.findByIdAndDelete(taskId, (err, deletedTask) => {
		if(err){
			console.log(err)
		} else {
			res.send(`${deletedTask} has been deleted`)
		}
	})
});





// START OF S32 ACTIVITY

// UserSchema and User model already created at the top



// Business Logic

// Register new user 
app.post("/users/signup", (req, res) => {

	User.findOne({name : req.body.email}, (err, result) => {

		if(result != null && result.email === req.body.email){
			
			return res.send(`Duplicate user found: ${err}`)
		
		} else {
			
			let newUser = new User({
				email: req.body.email,
				username: req.body.username,
				password: req.body.password,
				age: req.body.age
			});

			newUser.save((saveErr, savedUser) => {
				if(saveErr){
					return console.error(saveErr);
				} else {
					return res.status(200).send(`New user created: ${savedUser}`)
				};
			});
		};
	});
});



// Retrieve all users that registered

app.get("/users", (req, res) => {

	User.find({}, (err, result) => {
		
		if(err){
			
			return console.log(err);
		
		} else {
			
			return res.status(200).json({
				users: result
			});
		};
	});
});



// Updating User's Username
app.put("/users/update-user/:wildcard", (req, res) => {

	let userId = req.params.userId
	let username = req.body.username

	User.findByIdAndUpdate(userId, {username: username}, (err, updatedUser) => {
		if(err){
				console.log(err)
		} else {

				let updatedUser = User({
				email: req.body.email,
				username: req.body.username,
				password: req.body.password,
				age: req.body.age
			});

				res.send(`Congratulations the username has been updated: ${updatedUser}`);
		}
	});
});



// Delete User
app.delete("/users/archive-user/:userId", (req, res) => {

	let userId = req.params.userId;
	
	User.findByIdAndDelete(userId, (err, deletedUser) => {
		if(err){
			console.log(err)
		} else {
			res.send(`${deletedUser} has been deleted`)
		}
	})
});



app.listen(port, () => console.log(`Server running at port ${port}`));